package rsa;
import java.math.BigInteger;
import java.security.SecureRandom;


public class RSA {
	private static BigInteger N, d, e;

	private int bitlen;
	public static Encrypt encryptor;
	public static Decrypt decryptor;

  /** Create an instance that can both encrypt and decrypt. */
	public RSA(int bits) 
	{
	    bitlen = bits;
	    generateKeys();
	    decryptor=new Decrypt(d, N);
	}
	public RSA()
	{
		this(2048);
	}
	public void defineEnc(BigInteger pubblica, BigInteger modulo)
	{
		encryptor=new Encrypt(pubblica, modulo);
	}
	public static BigInteger encryptString(String message)
	{
		return encryptor.go(message);
	}
	public static String decryptMessage(BigInteger message)
	{
		return decryptor.go(message);
	}
  /** Generate a new public and private key set. */
	public synchronized void generateKeys() 
	{
		SecureRandom r = new SecureRandom();
	    BigInteger p = new BigInteger((bitlen-1) / 2, 100, r);
	    BigInteger q = new BigInteger(bitlen / 2, 100, r);
	    N = p.multiply(q);
	    BigInteger L = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
	    e = new BigInteger("65537");
	    while (L.gcd(e).intValue() > 1) {
	    	e = e.add(new BigInteger("2"));
	    }
	    d = e.modInverse(L);
	    defineEnc(e, N);
	}
	public static BigInteger getEncrypt()
	{
		return e;
	}
	public static BigInteger getModulus()
	{
		return N;
	}
	public static BigInteger getDecrypt()
	{
		return d;
	}
}