package rsa;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

final class Decrypt {
	private static BigInteger d;
	private static BigInteger N;
	public Decrypt(BigInteger privateKey, BigInteger modulo)
	{
		d=privateKey;
		N=modulo;
	}
	
	public static synchronized String go(BigInteger message) 
	{
	    BigInteger toDecr=goBI(message);
	    String result= new String(toDecr.toByteArray());
	    return result;
	}

	  /** Encrypt the given plaintext message. */
	public static synchronized BigInteger goBI(BigInteger message) 
	{
    	return message.modPow(d, N);
	}

}
