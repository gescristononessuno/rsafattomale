package crittografia;

import java.util.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import rsa.*;

public class Mein {
	private static final int BITLEN= 512;
	public static void main(String[] args)
	{
		RSA rsa=new RSA(BITLEN);
		Path percorsoDiv= Paths.get("/home/lasko/Desktop/Repos/rsafattomale/crittografia/src/crittografia/divina.txt");
		List<String> div = null;
		try {
			div=Files.lines(percorsoDiv)
							.collect(Collectors.toList());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		div.add("");
		//div.stream().forEach(System.out::println);
		List<BigInteger> cryptedDiv= null;
		cryptedDiv=div.stream()
				.filter(k-> k.length()>5)
				.filter(k-> k.length()<BITLEN/10)
				.map(k-> rsa.encryptString(k))
				.collect(Collectors.toList());
		//System.out.println(div.get(7004));
		//System.out.println(rsa.encryptString(div.get(7004)));
		//cryptedDiv.stream().forEach(System.out::println);
		/*List<String> decrypted= cryptedDiv.stream()
				.map(k->rsa.decryptMessage(k))
				.collect(Collectors.toList());*/
		//decrypted.stream().forEach(System.out::println);
		//System.out.println(div.get(7004));
		//System.out.println(cryptedDiv.get(7004));
		//System.out.println(decrypted.get(7004));
        String file = "cripted.txt";
        System.out.println("Writing to file: " + file);
        // Files.newBufferedWriter() uses UTF-8 encoding by default
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file))) 
        {
        	cryptedDiv.stream()
        		.forEach(k-> {
					try {
						writer.write(k.toString());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
        } // the file will be automatically closed
        catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        /* 
        System.out.println("Parametri");
        System.out.println("bitlen -> "+BITLEN);
        System.out.println("N -> "+rsa.getModulus());
        System.out.println("d -> "+rsa.getDecrypt());
        */
        Path percorsoCryp= Paths.get("/home/lasko/Desktop/Repos/rsafattomale/crittografia/cripted.txt");
		List<String> cryptS = null;
		try {
			cryptS=Files.lines(percorsoCryp)
							.collect(Collectors.toList());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<BigInteger> cryptBI=
				cryptS.stream()
				//.forEach(k-> System.out.println(k.toString()));
				.map(k-> new BigInteger(k))
				.collect(Collectors.toList());
		List<String> decrypted= cryptBI.stream()
				.map(k->rsa.decryptMessage(k))
				.collect(Collectors.toList());
		decrypted.stream().forEach(System.out::println);
	}
}
